#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class Action {
public:
    Action (int hello) : hi(hello)
    {
    }
    int siemka(int cze)
    {
        std::cout << cze + hi << std::endl;
        return cze + hi;
    }

private:
    int hi;
};

class Timer {
public:
    Timer (long sleep, boost::function<void()> f)
    {
        boost::thread t(&Timer::worker, this, sleep, f);
        std::cout << "RACE CONDITION" << std::endl;
        t.join();
        std::cout << "waiting finished" << std::endl;
    }

    void worker(long sleep, boost::function<void()> f)
    {
        std::cout << "Sleeping for " << sleep << std::endl;
        boost::this_thread::sleep(boost::posix_time::seconds(sleep));
        f();
    }
};

int main(int argc, const char *argv[])
{
    Action act(5);
    Timer t(5, boost::bind(&Action::siemka, &act, 10));
    return 0;
}

